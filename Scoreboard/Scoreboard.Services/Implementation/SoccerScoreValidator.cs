﻿// <copyright file="SoccerScoreValidator.cs" company="TAL Insurance">
// Copyright (c) TAL Insurance. All rights reserved.
// </copyright>

namespace Scoreboard.Services.Implementation
{
    using System;
    using System.Collections.Generic;
    using Optional;
    using Scoreboard.Data;
    using Scoreboard.Services.Contracts;

    // 0 - Standing
    // 1 - Name
    // 2 - Games played
    // 3 - Wins
    // 4 - Losses
    // 5 - Draws
    // 6 - Goals for
    // 7 - Dash delimiter, ignored
    // 8 - Goals against
    // 9 - Points

    /// <summary>
    /// The soccer score validator class.
    /// </summary>
    public sealed class SoccerScoreValidator : IScoreValidator<SoccerScore>
    {
        private const int ColumnsRequired = 10;

        private const uint NameIndex = 1;

        private static readonly char[] ScoreboardSeparator = new char[] { ' ', '\t' };

        private readonly int pointsPerWin = 3;

        private readonly int pointsPerLoss = 1;

        private readonly int pointsPerDraw = 0;

        private readonly IEnumerable<Func<string[], SoccerScore, bool>> validations;

        /// <summary>
        /// Initialises a new instance of the <see cref="SoccerScoreValidator"/> class.
        /// </summary>
        /// <param name="pointsPerWin">The points per win.</param>
        /// <param name="pointsPerLoss">The points per loss.</param>
        /// <param name="pointsPerDraw">The points per draw.</param>
        public SoccerScoreValidator(int pointsPerWin, int pointsPerLoss, int pointsPerDraw)
        {
            this.pointsPerWin = pointsPerWin;
            this.pointsPerLoss = pointsPerLoss;
            this.pointsPerDraw = pointsPerDraw;

            this.validations = new List<Func<string[], SoccerScore, bool>>
            {
                (a, b) => ValidateStanding(a, b),
                (a, b) => ValidateName(a, b),
                (a, b) => this.ValidateGamesPlayed(a, b),
                (a, b) => ValidatePoints(a, b),
            };
        }

        /// <inheritdoc/>
        public Option<SoccerScore> ValidateAndParseRawScore(string rawScore)
        {
            var nextRawScoresInColumns = rawScore.Split(ScoreboardSeparator, StringSplitOptions.RemoveEmptyEntries);

            if (nextRawScoresInColumns.Length != ColumnsRequired)
            {
                return Option.None<SoccerScore>();
            }

            return this.ValidateScore(nextRawScoresInColumns);
        }

        private static bool ValidateName(string[] rawScore, SoccerScore soccerScore)
        {
            soccerScore.TeamName = rawScore[NameIndex];
            return !string.IsNullOrEmpty(soccerScore.TeamName);
        }

        private static bool ValidatePoints(string[] rawValues, SoccerScore soccerScore)
        {
            bool isValid = true;
            isValid &= ValidateIntegralValue(rawValues[6], out uint goalsFor);
            isValid &= ValidateIntegralValue(rawValues[8], out uint goalsAgainst);

            soccerScore.GoalsFor = goalsFor;
            soccerScore.GoalsAgainst = goalsAgainst;

            return isValid;
        }

        private static bool ValidateStanding(string[] rawValues, SoccerScore soccerScoreDefinition)
        {
            var rawStanding = rawValues[0].Split(".", StringSplitOptions.RemoveEmptyEntries);
            if (rawStanding.Length != 1)
            {
                return false;
            }

            var isValid = ValidateIntegralValue(rawStanding[0], out uint standing);
            soccerScoreDefinition.Standing = standing;
            return isValid && standing > 0;
        }

        private static bool ValidateIntegralValue(string input, out uint value)
        {
            var wasValidIntegral = uint.TryParse(input, out value);
            return wasValidIntegral && value >= 0;
        }

        private Option<SoccerScore> ValidateScore(string[] nextRawScoresInColumns)
        {
            var soccerScore = new SoccerScore();
            foreach (var validation in this.validations)
            {
                if (!validation(nextRawScoresInColumns, soccerScore))
                {
                    return Option.None<SoccerScore>();
                }
            }

            return Option.Some(soccerScore);
        }

        private bool ValidateGamesPlayed(string[] rawValues, SoccerScore soccerScore)
        {
            bool isValid = true;
            isValid &= ValidateIntegralValue(rawValues[2], out uint matchesPlayed);
            isValid &= ValidateIntegralValue(rawValues[3], out uint wins);
            isValid &= ValidateIntegralValue(rawValues[4], out uint losses);
            isValid &= ValidateIntegralValue(rawValues[5], out uint draws);
            isValid &= ValidateIntegralValue(rawValues[9], out uint points);

            isValid &= matchesPlayed == wins + losses + draws;
            isValid &= points == (this.pointsPerWin * wins) + (this.pointsPerLoss * losses) + (this.pointsPerDraw * draws);

            soccerScore.MatchesPlayed = matchesPlayed;
            soccerScore.Wins = wins;
            soccerScore.Losses = losses;
            soccerScore.Draws = draws;
            soccerScore.Points = points;

            return isValid;
        }
    }
}
