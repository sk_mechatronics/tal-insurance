﻿// <copyright file="ScoreboardDatFileReader.cs" company="TAL Insurance">
// Copyright (c) TAL Insurance. All rights reserved.
// </copyright>

namespace Scoreboard.Services.Implementation
{
    using System.IO;
    using System.Threading.Tasks;

    /// <summary>
    /// The scoreboard file reader.
    /// </summary>
    public sealed class ScoreboardDatFileReader : Contracts.IScoreboardInputReader
    {
        private StreamReader streamReader;

        private bool isDisposed;

        /// <inheritdoc/>
        public async Task<string> GetNextScore()
        {
            if (this.streamReader == null || this.isDisposed)
            {
                throw new TalException("Source was not set.");
            }

            return await this.streamReader.ReadLineAsync();
        }

        /// <inheritdoc/>
        public void SetSource(string source)
        {
            if (this.streamReader != null)
            {
                this.Dispose();
            }

            this.streamReader = new StreamReader(File.OpenRead(source));
            this.isDisposed = false;
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            using (this.streamReader)
            {
            }

            this.isDisposed = true;
        }
    }
}
