﻿// <copyright file="ConsoleOutput.cs" company="TAL Insurance">
// Copyright (c) TAL Insurance. All rights reserved.
// </copyright>

namespace Scoreboard.Services.Implementation
{
    using Scoreboard.Services.Contracts;

    /// <summary>
    /// Outputs content to the console.
    /// </summary>
    public sealed class ConsoleOutput : IOutput
    {
        /// <inheritdoc/>
        public void Write(string content)
        {
            System.Console.WriteLine(content);
        }
    }
}
