﻿// <copyright file="SoccerScoreboardParser.cs" company="TAL Insurance">
// Copyright (c) TAL Insurance. All rights reserved.
// </copyright>

namespace Scoreboard.Services.Implementation
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Scoreboard.Data;
    using Scoreboard.Services.Contracts;

    /// <summary>
    /// The soccer score board parser.
    /// </summary>
    public sealed class SoccerScoreboardParser : IScoreboardParser<IList<SoccerScore>>, System.IDisposable
    {
        private readonly IScoreboardInputReader scoreboardInputReader;

        private readonly IScoreValidator<SoccerScore> soccerScoreValidator;

        /// <summary>
        /// Initialises a new instance of the <see cref="SoccerScoreboardParser"/> class.
        /// </summary>
        /// <param name="reader">The input reader.</param>
        /// <param name="soccerScoreValidator">The validator.</param>
        public SoccerScoreboardParser(IScoreboardInputReader reader, IScoreValidator<SoccerScore> soccerScoreValidator)
        {
            this.scoreboardInputReader = reader;
            this.soccerScoreValidator = soccerScoreValidator;
        }

        /// <summary>
        /// Gets the scoreboard.
        /// </summary>
        public IList<SoccerScore> Scoreboard { get; } = new List<SoccerScore>();

        /// <inheritdoc />
        public void Dispose()
        {
            this.scoreboardInputReader.Dispose();
        }

        /// <summary>
        /// Parse all the scores.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the result of the asynchronous operation.</returns>
        public async Task Parse()
        {
            await this.ReadScores();
            this.ValidateScoreboard();
        }

        private async Task ReadScores()
        {
            string rawScore = await this.scoreboardInputReader.GetNextScore();
            while (rawScore != null)
            {
                var validated = this.soccerScoreValidator.ValidateAndParseRawScore(rawScore);
                if (validated.HasValue)
                {
                    this.Scoreboard.Add(validated.ValueOr(default(SoccerScore)));
                }

                rawScore = await this.scoreboardInputReader.GetNextScore();
            }
        }

        private void ValidateScoreboard()
        {
            this.ValidateScoreboardCount();
            this.ValidateStandings();
            this.ValidateNames();
        }

        private void ValidateScoreboardCount()
        {
            if (!this.Scoreboard.Any())
            {
                throw new TalException("No valid scores were read");
            }
        }

        private void ValidateStandings()
        {
            if (this.Scoreboard.GroupBy(i => i.Standing).Any(grp => grp.Count() > 1))
            {
                throw new TalException("Multiple teams with same standings were read");
            }
        }

        private void ValidateNames()
        {
            if (this.Scoreboard.GroupBy(i => i.TeamName).Any(grp => grp.Count() > 1))
            {
                throw new TalException("Multiple teams with the same names were read");
            }
        }
    }
}
