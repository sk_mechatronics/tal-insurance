﻿// <copyright file="SmallestGoalDifferenceAnalyser.cs" company="TAL Insurance">
// Copyright (c) TAL Insurance. All rights reserved.
// </copyright>

namespace Scoreboard.Services.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Scoreboard.Data;
    using Scoreboard.Services.Contracts;

    /// <summary>
    /// Analyses the difference between the goals and prints out the team with the smallest difference.
    /// </summary>
    public sealed class SmallestGoalDifferenceAnalyser : IScoreboardAnalyser<IList<SoccerScore>>
    {
        /// <summary>
        /// The output.
        /// </summary>
        private readonly IOutput output;

        /// <summary>
        /// Initialises a new instance of the <see cref="SmallestGoalDifferenceAnalyser"/> class.
        /// </summary>
        /// <param name="output">The output.</param>
        public SmallestGoalDifferenceAnalyser(IOutput output)
        {
            this.output = output;
        }

        /// <inheritdoc/>
        public void Analyse(IScoreboardParser<IList<SoccerScore>> parser)
        {
            var differences = parser.Scoreboard.Select(i => new
                    {
                        Difference = Math.Abs((int)i.GoalsFor - (int)i.GoalsAgainst),
                        i.Standing,
                        i.TeamName,
                    })
                .OrderBy(i => i.Difference).ThenBy(i => i.Standing);

            var smallestDifference = differences.First();
            this.output.Write($"The team with the smallest goal difference was {smallestDifference.TeamName}");
        }
    }
}
