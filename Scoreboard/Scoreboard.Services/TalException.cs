﻿// <copyright file="TalException.cs" company="TAL Insurance">
// Copyright (c) TAL Insurance. All rights reserved.
// </copyright>

namespace Scoreboard.Services
{
    /// <summary>
    /// The TAL Exception.
    /// </summary>
    public class TalException : System.Exception
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="TalException"/> class.
        /// </summary>
        /// <param name="message">The error message.</param>
        public TalException(string message)
            : base(message)
        {
        }
    }
}
