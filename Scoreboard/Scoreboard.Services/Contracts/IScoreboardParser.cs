﻿// <copyright file="IScoreboardParser.cs" company="TAL Insurance">
// Copyright (c) TAL Insurance. All rights reserved.
// </copyright>

namespace Scoreboard.Services.Contracts
{
    /// <summary>
    /// The scoreboard parser interface.
    /// </summary>
    /// <typeparam name="T">The type of scoreboard.</typeparam>
    public interface IScoreboardParser<T>
    {
        /// <summary>
        /// Gets the scoreboard.
        /// </summary>
        T Scoreboard { get; }

        /// <summary>
        /// Parse the input.
        /// </summary>
        /// <returns>The <see cref="Task"/>.</returns>
        System.Threading.Tasks.Task Parse();
    }
}
