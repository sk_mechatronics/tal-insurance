﻿// <copyright file="IScoreboardInputReader.cs" company="TAL Insurance">
// Copyright (c) TAL Insurance. All rights reserved.
// </copyright>

namespace Scoreboard.Services.Contracts
{
    /// <summary>
    /// The input reader for any score board.
    /// </summary>
    public interface IScoreboardInputReader : System.IDisposable
    {
        /// <summary>
        /// Set the source for reading - can be a file path or URI.
        /// </summary>
        /// <param name="source">The source.</param>
        void SetSource(string source);

        /// <summary>
        /// Gets the next score.
        /// </summary>
        /// <returns>A <see cref="System.Threading.Tasks.Task{string}/>" representing the next raw score.</returns>
        System.Threading.Tasks.Task<string> GetNextScore();
    }
}
