﻿// <copyright file="IScoreValidator.cs" company="TAL Insurance">
// Copyright (c) TAL Insurance. All rights reserved.
// </copyright>

namespace Scoreboard.Services.Contracts
{
    /// <summary>
    /// The score validator interface.
    /// </summary>
    /// <typeparam name="T">The <see cref="{T}"/> type of score.</typeparam>
    public interface IScoreValidator<T>
    {
        /// <summary>
        /// Validate and parse a raw score.
        /// </summary>
        /// <param name="raw">The raw score.</param>
        /// <returns>An option with a value if it's valid, otherwise None.</returns>
        Optional.Option<T> ValidateAndParseRawScore(string raw);
    }
}
