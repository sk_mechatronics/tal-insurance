﻿// <copyright file="IOutput.cs" company="TAL Insurance">
// Copyright (c) TAL Insurance. All rights reserved.
// </copyright>

namespace Scoreboard.Services.Contracts
{
    /// <summary>
    /// The output interface.
    /// </summary>
    public interface IOutput
    {
        /// <summary>
        /// Write content.
        /// </summary>
        /// <param name="content">The content.</param>
        void Write(string content);
    }
}
