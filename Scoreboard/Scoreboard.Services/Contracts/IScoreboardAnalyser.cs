﻿// <copyright file="IScoreboardAnalyser.cs" company="TAL Insurance">
// Copyright (c) TAL Insurance. All rights reserved.
// </copyright>

namespace Scoreboard.Services.Contracts
{
    /// <summary>
    /// The score board analyser.
    /// </summary>
    /// <typeparam name="T">The type of scoreboard.</typeparam>
    public interface IScoreboardAnalyser<T>
    {
        /// <summary>
        /// Analyse the scoreboard.
        /// </summary>
        /// <param name="parser">The parser with the result.</param>
        void Analyse(IScoreboardParser<T> parser);
    }
}
