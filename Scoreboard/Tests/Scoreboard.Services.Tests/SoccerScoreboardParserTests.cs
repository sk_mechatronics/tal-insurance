﻿// <copyright file="SoccerScoreboardParserTests.cs" company="TAL Insurance">
// Copyright (c) TAL Insurance. All rights reserved.
// </copyright>

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Scoreboard.Services.Contracts;

namespace Scoreboard.Services.Tests
{
    using System;
    using System.Threading.Tasks;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    using Optional;
    using Scoreboard.Data;
    using Scoreboard.Services.Contracts;
    using Scoreboard.Services.Implementation;

    [TestFixture]
    public class SoccerScoreboardParserTests
    {
        /// <summary>
        /// When no data is available (e.g. file is empty).
        /// </summary>
        [Test]
        public void ShouldRejectNoData()
        {
            var inputReader = new Mock<IScoreboardInputReader>();
            var soccerScoreValidator = new Mock<IScoreValidator<SoccerScore>>();
            var parser = new SoccerScoreboardParser(inputReader.Object, soccerScoreValidator.Object);

            inputReader.Setup(i => i.GetNextScore()).ReturnsAsync(default(string));
            Func<Task> func = async () => await parser.Parse();
            func.Should().Throw<TalException>().WithMessage("No valid scores were read");

            parser.Dispose();
        }

        [Test]
        public void ShouldRejectEmptyScoreboard()
        {
            var inputReader = new TestSoccerScoreboardReader(string.Empty);
            var soccerScoreValidator = new Mock<IScoreValidator<SoccerScore>>();
            soccerScoreValidator.Setup(i => i.ValidateAndParseRawScore(string.Empty)).Returns(Option.None<SoccerScore>);

            var parser = new SoccerScoreboardParser(inputReader, soccerScoreValidator.Object);
            Func<Task> func = async () => await parser.Parse();
            func.Should().Throw<TalException>().WithMessage("No valid scores were read");

            parser.Dispose();
        }

        [Test]
        public async Task ShouldStoreScores()
        {
            var inputReader = new TestSoccerScoreboardReader(string.Empty, string.Empty);
            var soccerScoreValidator = new Mock<IScoreValidator<SoccerScore>>();

            soccerScoreValidator.SetupSequence(i => i.ValidateAndParseRawScore(It.IsAny<string>()))
                .Returns(Option.Some(new SoccerScore { Standing = 1, TeamName = "One" }))
                .Returns(Option.Some(new SoccerScore { Standing = 2, TeamName = "Two" }));

            var parser = new SoccerScoreboardParser(inputReader, soccerScoreValidator.Object);
            await parser.Parse();

            parser.Scoreboard.Count.Should().Be(2);
            parser.Scoreboard[0].TeamName.Should().Be("One");
            parser.Scoreboard[1].TeamName.Should().Be("Two");
        }

        [Test]
        public void ShouldNotAllowSameStandings()
        {
            var inputReader = new TestSoccerScoreboardReader(string.Empty, string.Empty);
            var soccerScoreValidator = new Mock<IScoreValidator<SoccerScore>>();

            soccerScoreValidator.SetupSequence(i => i.ValidateAndParseRawScore(It.IsAny<string>()))
                .Returns(Option.Some(new SoccerScore { Standing = 1, TeamName = "One" }))
                .Returns(Option.Some(new SoccerScore { Standing = 1, TeamName = "Two" }));

            var parser = new SoccerScoreboardParser(inputReader, soccerScoreValidator.Object);
            Func<Task> func = async () => await parser.Parse();
            func.Should().Throw<TalException>().WithMessage("Multiple teams with same standings were read");
        }

        [Test]
        public void ShouldNotAllowSameTeamnames()
        {
            var inputReader = new TestSoccerScoreboardReader(string.Empty, string.Empty);
            var soccerScoreValidator = new Mock<IScoreValidator<SoccerScore>>();

            soccerScoreValidator.SetupSequence(i => i.ValidateAndParseRawScore(It.IsAny<string>()))
                .Returns(Option.Some(new SoccerScore { Standing = 1, TeamName = "One" }))
                .Returns(Option.Some(new SoccerScore { Standing = 2, TeamName = "One" }));

            var parser = new SoccerScoreboardParser(inputReader, soccerScoreValidator.Object);
            Func<Task> func = async () => await parser.Parse();
            func.Should().Throw<TalException>().WithMessage("Multiple teams with the same names were read");
        }
    }
}

/// <summary>
/// A test class to mock out a reader.
/// </summary>
internal class TestSoccerScoreboardReader : IScoreboardInputReader
{
    /// <summary>
    /// The sequence.
    /// </summary>
    private readonly IList<string> sequence;

    /// <summary>
    /// The index to keep track of the sequence.
    /// </summary>
    private int index = 0;

    /// <summary>
    /// Initialises a new instance of the <see cref="TestSoccerScoreboardReader"/> class.
    /// </summary>
    /// <param name="rawScores">The raw scores to initialise the sequence with.</param>
    public TestSoccerScoreboardReader(params string[] rawScores)
    {
        this.sequence = rawScores.ToList();
    }

    /// <inheritdoc/>
    public void Dispose()
    {
    }

    /// <inheritdoc/>
    public Task<string> GetNextScore()
    {
        if (this.index >= this.sequence.Count)
        {
            return Task.FromResult<string>(null);
        }

        return Task.FromResult(this.sequence[this.index++]);
    }

    /// <inheritdoc/>
    public void SetSource(string source)
    {
    }
}