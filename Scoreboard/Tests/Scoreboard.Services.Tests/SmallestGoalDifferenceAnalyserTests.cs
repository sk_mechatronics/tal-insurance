﻿// <copyright file="SmallestGoalDifferenceAnalyserTests.cs" company="TAL Insurance">
// Copyright (c) TAL Insurance. All rights reserved.
// </copyright>

namespace Scoreboard.Services.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    using Scoreboard.Data;
    using Scoreboard.Services.Contracts;
    using Scoreboard.Services.Implementation;

    [TestFixture]
    internal class SmallestGoalDifferenceAnalyserTests
    {
        private CaptureOutput captureOutput = new CaptureOutput();

        private Mock<IScoreboardParser<IList<SoccerScore>>> soccerScoreboardParser;

        private SmallestGoalDifferenceAnalyser smallestDifferenceAnalyser;

        [SetUp]
        public void SetUp()
        {
            this.soccerScoreboardParser = new Mock<IScoreboardParser<IList<SoccerScore>>>();
        }

        [Test]
        public void ShouldGetSmallestWhenForGreaterThanAgainst()
        {
            this.soccerScoreboardParser.SetupSequence(i => i.Scoreboard)
                .Returns(new List<SoccerScore>
                {
                    new SoccerScore { TeamName = "Arsenal", GoalsFor = 40, GoalsAgainst = 39 },
                    new SoccerScore { TeamName = "Southampton", GoalsFor = 40, GoalsAgainst = 38 },
                });
            this.smallestDifferenceAnalyser =
                new SmallestGoalDifferenceAnalyser(this.captureOutput);

            this.smallestDifferenceAnalyser.Analyse(this.soccerScoreboardParser.Object);
            var smallest = this.captureOutput.GetLastContentWritten();
            smallest.Should().Be("The team with the smallest goal difference was Arsenal");
        }

        [Test]
        public void ShouldGetSmallestWhenAgainstGreaterThanFor()
        {
            this.soccerScoreboardParser.SetupSequence(i => i.Scoreboard)
                .Returns(new List<SoccerScore>
                {
                    new SoccerScore { TeamName = "Arsenal", GoalsFor = 40, GoalsAgainst = 30 },
                    new SoccerScore { TeamName = "Southampton", GoalsFor = 40, GoalsAgainst = 41 },
                });
            this.smallestDifferenceAnalyser =
                new SmallestGoalDifferenceAnalyser(this.captureOutput);

            this.smallestDifferenceAnalyser.Analyse(this.soccerScoreboardParser.Object);
            var smallest = this.captureOutput.GetLastContentWritten();
            smallest.Should().Be("The team with the smallest goal difference was Southampton");
        }

        [Test]
        public void ShouldGetFirstWhenDifferenceIsTheSame()
        {
            this.soccerScoreboardParser.SetupSequence(i => i.Scoreboard)
                .Returns(new List<SoccerScore>
                {
                    new SoccerScore { Standing = 2, TeamName = "Arsenal", GoalsFor = 40, GoalsAgainst = 39 },
                    new SoccerScore { Standing = 1, TeamName = "Southampton", GoalsFor = 40, GoalsAgainst = 39 },
                });
            this.smallestDifferenceAnalyser =
                new SmallestGoalDifferenceAnalyser(this.captureOutput);

            this.smallestDifferenceAnalyser.Analyse(this.soccerScoreboardParser.Object);
            var smallest = this.captureOutput.GetLastContentWritten();
            smallest.Should().Be("The team with the smallest goal difference was Southampton");
        }
    }

    /// <summary>
    /// A class to capture output for testing.
    /// </summary>
    internal class CaptureOutput : IOutput
    {
        private IList<string> contentsWritten = new List<string>();

        /// <inheritdoc/>
        public void Write(string content)
        {
            this.contentsWritten.Add(content);
        }

        public string GetLastContentWritten()
        {
            return this.contentsWritten.Last();
        }
    }
}
