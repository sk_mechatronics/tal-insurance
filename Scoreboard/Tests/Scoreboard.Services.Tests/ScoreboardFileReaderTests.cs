﻿// <copyright file="ScoreboardFileReaderTests.cs" company="TAL Insurance">
// Copyright (c) TAL Insurance. All rights reserved.
// </copyright>

namespace Scoreboard.Services.Tests
{
    using System.IO;
    using System.Threading.Tasks;
    using NUnit.Framework;

    using Scoreboard.Services;
    using Scoreboard.Services.Contracts;
    using Scoreboard.Services.Implementation;

    [TestFixture]
    internal class ScoreboardFileReaderTests
    {
        private IScoreboardInputReader fileReader;

        [SetUp]
        public void SetUp()
        {
            this.fileReader = new ScoreboardDatFileReader();
        }

        [Test]
        public async Task ShouldNotReadWithEmptySource()
        {
            try
            {
                await this.fileReader.GetNextScore();
                Assert.Fail();
            }
            catch (TalException)
            {
                Assert.Pass();
            }
        }

        [Test]
        public async Task ShouldCleanup()
        {
            File.Create("source3").Close();
            try
            {
                this.fileReader.SetSource("source3");
                this.fileReader.Dispose();
                await this.fileReader.GetNextScore();
                Assert.Fail();
            }
            catch (TalException)
            {
                Assert.Pass();
            }
        }

        [Test]
        public async Task ShouldReadNextLine()
        {
            SetupFileForRead("source4");
            this.fileReader.SetSource("source4");
            var nextLine = await this.fileReader.GetNextScore();
            Assert.AreEqual(nextLine, "Line 1");
        }

        [TearDown]
        public void TearDown()
        {
            this.fileReader.Dispose();
        }

        private static void SetupFileForRead(string filePath)
        {
            using (var stream = File.Create(filePath))
            {
                var streamWriter = new StreamWriter(stream);
                streamWriter.WriteLine("Line 1");
                streamWriter.WriteLine("Line 2");
                streamWriter.WriteLine("Line 3");
                streamWriter.Flush();
            }
        }
    }
}
