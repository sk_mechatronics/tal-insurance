﻿// <copyright file="SoccerScoreValidatorTests.cs" company="TAL Insurance">
// Copyright (c) TAL Insurance. All rights reserved.
// </copyright>

namespace Scoreboard.Services.Tests
{
    using FluentAssertions;
    using NUnit.Framework;
    using Optional;
    using Optional.Unsafe;
    using Scoreboard.Data;
    using Scoreboard.Services.Implementation;

    [TestFixture]
    internal class SoccerScoreValidatorTests
    {
        private SoccerScoreValidator parser;

        [SetUp]
        public void SetUp()
        {
            this.parser = new SoccerScoreValidator(3, 1, 0);
        }

        [Test]
        [TestCase("    1. Arsenal         38    26   9   3    79  -  36    87")]
        public void ShouldReadValidData(string input)
        {
            var nextScore = this.parser.ValidateAndParseRawScore(input);
            var expectedScore = new SoccerScore
            {
                Standing = 1,
                MatchesPlayed = 38,
                Wins = 26,
                Losses = 9,
                Draws = 3,
                GoalsFor = 79,
                GoalsAgainst = 36,
                Points = 87,
                TeamName = "Arsenal",
            };

            nextScore.ValueOrFailure().Should().BeEquivalentTo(expectedScore);
        }

        [Test]
        [TestCase("    -1. Arsenal          38    26   9   3    79  -  36    87", Description = "Negative standing")]
        [TestCase("    0. Arsenal          38    26   9   3    79  -  36    87", Description = "Zero standing")]
        [TestCase("-------------------------------------------------------", Description = "Separator should be ignored")]
        [TestCase("    1. Arsenal          38    26   9   3    79  36    87", Description = "Goal separator missing")]
        [TestCase("    1.                   38    26   9   3    79  -  36    87", Description = "Name missing")]
        [TestCase("    1. Arsenal           -38   26   9   3    79  -  36    87", Description = "Games played negative")]
        [TestCase("    1. Arsenal           38    -26   9   3    79  -  36    87", Description = "Wins negative")]
        [TestCase("    1. Arsenal           38    26   -9   3    79  -  36    87", Description = "Losses negative")]
        [TestCase("    1. Arsenal           38    26   9   -3    79  -  36    87", Description = "Draws negative")]
        [TestCase("    1. Arsenal           38    26   9   3    -79  -  36    87", Description = "For negative")]
        [TestCase("    1. Arsenal           38    26   9   3    79  -  -36    87", Description = "Against negative")]
        [TestCase("    1. Arsenal           38    26   9   3    79  -  36    -87", Description = "Points negative")]
        public void ShouldRejectInvalidData(string input)
        {
            var nextScore = this.parser.ValidateAndParseRawScore(input);
            nextScore.Should().Be(Option.None<SoccerScore>());
        }

        [Test]
        [TestCase("    1. Arsenal         37    26   9   3    79  -  36    87", Description = "Total games are short")]
        [TestCase("    1. Arsenal         38    25   9   3    79  -  36    87", Description = "Wins are short")]
        [TestCase("    1. Arsenal         38    26   8   3    79  -  36    87", Description = "Losses are short")]
        [TestCase("    1. Arsenal         38    26   9   2    79  -  36    87", Description = "Draws are short")]
        public void ShouldMatchGamesWithWinsLossesAndDraws(string input)
        {
            var nextScore = this.parser.ValidateAndParseRawScore(input);
            nextScore.Should().Be(Option.None<SoccerScore>());
        }

        [Test]
        [TestCase("    1. Arsenal         38    26   9   3    79  -  36    86", Description = "Total points are incorrect")]
        public void ShouldMatchPoints(string input)
        {
            var nextScore = this.parser.ValidateAndParseRawScore(input);
            nextScore.Should().Be(Option.None<SoccerScore>());
        }

        [Test]
        [TestCase("    1. Arsenal         0    0   0    0   0 -  0    0")]
        public void ShouldAccommodateStartingState(string input)
        {
            var nextScore = this.parser.ValidateAndParseRawScore(input);
            nextScore.HasValue.Should().BeTrue();
        }
    }
}
