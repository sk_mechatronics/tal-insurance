﻿// <copyright file="SoccerScore.cs" company="TAL Insurance">
// Copyright (c) TAL Insurance. All rights reserved.
// </copyright>

namespace Scoreboard.Data
{
    /// <summary>
    /// The soccer score data.
    /// </summary>
    public class SoccerScore
    {
        /// <summary>
        /// Gets or sets the team's overall standing.
        /// </summary>
        public uint Standing { get; set; }

        /// <summary>
        /// Gets or sets the team name.
        /// </summary>
        public string TeamName { get; set; }

        /// <summary>
        /// Gets or sets the matches played for this team.
        /// </summary>
        public uint MatchesPlayed { get; set; }

        /// <summary>
        /// Gets or sets the wins for this team.
        /// </summary>
        public uint Wins { get; set; }

        /// <summary>
        /// Gets or sets the nuber of losses for this team.
        /// </summary>
        public uint Losses { get; set; }

        /// <summary>
        /// Gets or sets the number of draws for this team.
        /// </summary>
        public uint Draws { get; set; }

        /// <summary>
        /// Gets or sets the goals this team scored.
        /// </summary>
        public uint GoalsFor { get; set; }

        /// <summary>
        /// Gets or sets the goals scored against this team.
        /// </summary>
        public uint GoalsAgainst { get; set; }

        /// <summary>
        /// Gets or sets the total points for this team in the leaderboard.
        /// </summary>
        public uint Points { get; set; }
    }
}
