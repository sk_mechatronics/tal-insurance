﻿// <copyright file="Runner.cs" company="TAL Insurance">
// Copyright (c) TAL Insurance. All rights reserved.
// </copyright>

namespace Scoreboard.Console
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using Scoreboard.Data;
    using Scoreboard.Services;
    using Scoreboard.Services.Contracts;
    using Scoreboard.Services.Implementation;

    using SoccerScoreboard = System.Collections.Generic.IList<Scoreboard.Data.SoccerScore>;

    /// <summary>
    /// The runner.
    /// </summary>
    public class Runner
    {
        private const int PointsForWin = 3;

        private const int PointsPerLoss = 1;

        private const int PointsPerDraw = 0;

        private const string DatExtension = ".dat";

        private readonly IOutput output;

        private readonly IScoreboardParser<SoccerScoreboard> scoreboardParser;

        private readonly IEnumerable<IScoreboardAnalyser<SoccerScoreboard>> scoreboardAnalysers;

        private readonly IScoreboardInputReader scoreboardInputReader;

        private readonly IScoreValidator<SoccerScore> scoreboardValidator;

        /// <summary>
        /// Initialises a new instance of the <see cref="Runner"/> class.
        /// This acts as dependency injection.
        /// </summary>
        public Runner()
        {
            this.output = new ConsoleOutput();
            this.scoreboardInputReader = new ScoreboardDatFileReader();
            this.scoreboardValidator = new SoccerScoreValidator(PointsForWin, PointsPerLoss, PointsPerDraw);
            this.scoreboardParser = new SoccerScoreboardParser(this.scoreboardInputReader, this.scoreboardValidator);
            this.scoreboardAnalysers = new List<IScoreboardAnalyser<SoccerScoreboard>>
            {
                new SmallestGoalDifferenceAnalyser(this.output),
            };
        }

        /// <summary>
        /// The main.
        /// </summary>
        /// <param name="args">The args.</param>
        public static void Main(string[] args)
        {
            var runner = new Runner();
            runner.Execute(args);
        }

        private void Execute(string[] args)
        {
            try
            {
                this.ExecuteLogic(args);
            }
            catch (TalException e)
            {
                Console.WriteLine($"An error occurred trying to analyse the file.\r\n{e.Message}\r\n{e.StackTrace}");
                this.PressAnyKeyAndExit(ExitCodes.TalException);
            }
            catch (Exception e)
            {
                Console.WriteLine($"The application encountered a severe error.\r\n{e.Message}\r\n{e.StackTrace}");
                this.PressAnyKeyAndExit(ExitCodes.FatalUnknown);
            }
        }

        private void ExecuteLogic(string[] args)
        {
            var path = this.ReadFileLocation(args);
            this.ValidatePath(path);
            this.OpenSource(path);
            Task.WaitAll(this.Analyse());
            this.PressAnyKeyAndExit(ExitCodes.Success);
        }

        private async Task Analyse()
        {
            await this.scoreboardParser.Parse();

            foreach (var analyser in this.scoreboardAnalysers)
            {
                analyser.Analyse(this.scoreboardParser);
            }
        }

        private string ReadFileLocation(string[] args)
        {
            string path;
            if (args.Length >= 1)
            {
                Console.WriteLine("Using path from supplied arguments.");
                path = args[0];
            }
            else
            {
                Console.WriteLine("Please enter the location of the DAT file.");
                path = Console.ReadLine();
            }

            return path;
        }

        private void ValidatePath(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                Console.WriteLine("The path entered was not valid.");
                this.PressAnyKeyAndExit(ExitCodes.PathNullOrEmpty);
            }

            if (Path.GetExtension(path).ToLower() != DatExtension)
            {
                Console.WriteLine("The file should be a DAT file.");
                this.PressAnyKeyAndExit(ExitCodes.NotDatFile);
            }
        }

        private void OpenSource(string path)
        {
            try
            {
                this.scoreboardInputReader.SetSource(path);
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("The file was not found.");
                this.PressAnyKeyAndExit(ExitCodes.FileNotFound);
            }
        }

        private void PressAnyKeyAndExit(ExitCodes exitCode)
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            Environment.Exit((int)exitCode);
        }
    }
}
