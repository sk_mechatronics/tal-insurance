﻿// <copyright file="ExitCodes.cs" company="TAL Insurance">
// Copyright (c) TAL Insurance. All rights reserved.
// </copyright>

namespace Scoreboard.Console
{
    /// <summary>
    /// The exit codes for the console runner.
    /// </summary>
    internal enum ExitCodes
    {
        /// <summary>
        /// No error - placeholder to consume 0th index in enum.
        /// </summary>
        Success,

        /// <summary>
        /// The path was null or empty
        /// </summary>
        PathNullOrEmpty,

        /// <summary>
        /// The file was not a dat file.
        /// </summary>
        NotDatFile,

        /// <summary>
        /// The file was not found.
        /// </summary>
        FileNotFound,

        /// <summary>
        /// Logic exception
        /// </summary>
        TalException,

        /// <summary>
        /// An unknown fatal exception (catch all).
        /// </summary>
        FatalUnknown,
    }
}
